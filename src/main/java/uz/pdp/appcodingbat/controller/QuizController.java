package uz.pdp.appcodingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.entity.Quiz;
import uz.pdp.appcodingbat.entity.Section;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.QuizDto;
import uz.pdp.appcodingbat.payload.SectionDto;
import uz.pdp.appcodingbat.service.QuizService;
import uz.pdp.appcodingbat.service.SectionService;


@RestController
@RequestMapping(value = "/api/quiz")
public class QuizController {
    @Autowired
    QuizService quizService;

    /**
     * @param quizDto
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody QuizDto quizDto) {
        ApiResponse apiResponse = quizService.add(quizDto);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List < Quiz>>
     */

    @GetMapping
    public ResponseEntity<?> getAll(Integer page) {
        Page<Quiz> all = quizService.getAll(page);

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<Quiz>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        Quiz quiz = quizService.getById(id);

        return ResponseEntity.ok(quiz);
    }

    /**
     * @param quizDto
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody QuizDto quizDto, @PathVariable Integer id) {
        ApiResponse apiResponse = quizService.edit(quizDto, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = quizService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
