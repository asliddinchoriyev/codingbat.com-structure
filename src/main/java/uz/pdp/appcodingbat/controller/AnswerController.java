package uz.pdp.appcodingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.service.AnswerService;


@RestController
@RequestMapping(value = "/api/answer")
public class AnswerController {
    @Autowired
    AnswerService answerService;

    /**
     * @param answer
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody Answer answer) {
        ApiResponse apiResponse = answerService.add(answer);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List <Answer>>
     */

    @GetMapping
    public ResponseEntity<?> getAll(Integer page) {
        Page<Answer> all = answerService.getAll(page);

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<Address>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        Answer answer = answerService.getById(id);

        return ResponseEntity.ok(answer);
    }

    /**
     * @param comingAnswer
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody Answer comingAnswer, @PathVariable Integer id) {
        ApiResponse apiResponse = answerService.edit(comingAnswer, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = answerService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
