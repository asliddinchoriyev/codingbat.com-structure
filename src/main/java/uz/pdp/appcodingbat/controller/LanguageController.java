package uz.pdp.appcodingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.service.AnswerService;
import uz.pdp.appcodingbat.service.LanguageService;


@RestController
@RequestMapping(value = "/api/language")
public class LanguageController {
    @Autowired
    LanguageService languageService;

    /**
     * @param language
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody Language language) {
        ApiResponse apiResponse = languageService.add(language);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List < Language>>
     */

    @GetMapping
    public ResponseEntity<?> getAll(Integer page) {
        Page<Language> all = languageService.getAll(page);

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<Language>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        Language language = languageService.getById(id);

        return ResponseEntity.ok(language);
    }

    /**
     * @param comingLanguage
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody Language comingLanguage, @PathVariable Integer id) {
        ApiResponse apiResponse = languageService.edit(comingLanguage, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = languageService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
