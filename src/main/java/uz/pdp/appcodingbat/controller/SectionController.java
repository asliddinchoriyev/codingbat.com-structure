package uz.pdp.appcodingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.entity.Section;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.SectionDto;
import uz.pdp.appcodingbat.service.SectionService;


@RestController
@RequestMapping(value = "/api/section")
public class SectionController {
    @Autowired
    SectionService sectionService;

    /**
     * @param sectionDto
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody SectionDto sectionDto) {
        ApiResponse apiResponse = sectionService.add(sectionDto);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List < Section>>
     */

    @GetMapping
    public ResponseEntity<?> getAll(Integer page) {
        Page<Section> all = sectionService.getAll(page);

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<Section>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        Section section = sectionService.getById(id);

        return ResponseEntity.ok(section);
    }

    /**
     * @param sectionDto
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody SectionDto sectionDto, @PathVariable Integer id) {
        ApiResponse apiResponse = sectionService.edit(sectionDto, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = sectionService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
