package uz.pdp.appcodingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.UserDto;
import uz.pdp.appcodingbat.service.UserService;


@RestController
@RequestMapping(value = "/api/user")
public class UserController {
    @Autowired
    UserService userService;

    /**
     * @param userDto
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.add(userDto);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List < User>>
     */

    @GetMapping
    public ResponseEntity<?> getAll(Integer page) {
        Page<User> all = userService.getAll(page);

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<User>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        User user = userService.getById(id);

        return ResponseEntity.ok(user);
    }

    /**
     * @param userDto
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody UserDto userDto, @PathVariable Integer id) {
        ApiResponse apiResponse = userService.edit(userDto, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = userService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
