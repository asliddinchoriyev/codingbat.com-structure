package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuizDto {
    private String name;
    private Integer answerId;
    private Integer sectionId;
    private Set<Integer> solvedUserSet;

}
