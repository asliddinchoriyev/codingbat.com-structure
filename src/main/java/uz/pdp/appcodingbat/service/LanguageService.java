package uz.pdp.appcodingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.repository.AnswerRepository;
import uz.pdp.appcodingbat.repository.LanguageRepository;

import java.util.Optional;

@Service
public class LanguageService {
    @Autowired
    LanguageRepository languageRepository;

    public ApiResponse add(Language language) {

        boolean existsByName = languageRepository.existsByName(language.getName());

        if (existsByName)
            return new ApiResponse("This language already added", false);

        languageRepository.save(language);
        return new ApiResponse("The language added", true);
    }

    public Page<Language> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<Language> all = languageRepository.findAll(pageable);
        return all;
    }

    public Language getById(Integer id) {
        Optional<Language> optionalLanguage = languageRepository.findById(id);

        return optionalLanguage.orElse(null);
    }

    public ApiResponse edit(Language comingLanguage, Integer id) {

        Optional<Language> optionalLanguage = languageRepository.findById(id);

        if (optionalLanguage.isEmpty())
            return new ApiResponse("The language not found", false);

        boolean existsByName = languageRepository.existsByName(comingLanguage.getName());
        if (existsByName)
            return new ApiResponse("This language already exist", false);

        Language language = optionalLanguage.get();
        language.setName(comingLanguage.getName());

        languageRepository.save(language);

        return new ApiResponse("The language edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<Language> optionalLanguage = languageRepository.findById(id);

        if (optionalLanguage.isEmpty())
            return new ApiResponse("The language not found", false);

        languageRepository.deleteById(id);
        return new ApiResponse("The language deleted", true);
    }
}
