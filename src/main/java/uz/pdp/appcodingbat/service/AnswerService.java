package uz.pdp.appcodingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.repository.AnswerRepository;

import java.util.Optional;

@Service
public class AnswerService {
    @Autowired
    AnswerRepository answerRepository;

    public ApiResponse add(Answer answer) {

        boolean existsByResult = answerRepository.existsByResult(answer.getResult());

        if (existsByResult)
            return new ApiResponse("This answer already added", false);

        answerRepository.save(answer);
        return new ApiResponse("The answer added", true);
    }

    public Page<Answer> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<Answer> all = answerRepository.findAll(pageable);
        return all;
    }

    public Answer getById(Integer id) {
        Optional<Answer> optionalAnswer = answerRepository.findById(id);

        return optionalAnswer.orElse(null);
    }

    public ApiResponse edit(Answer comingAnswer, Integer id) {

        Optional<Answer> optionalAnswer = answerRepository.findById(id);

        if (optionalAnswer.isEmpty())
            return new ApiResponse("The answer not found", false);

        boolean existsByResult = answerRepository.existsByResult(comingAnswer.getResult());
        if (existsByResult)
            return new ApiResponse("This answer already exist", false);

        Answer answer = optionalAnswer.get();
        answer.setResult(comingAnswer.getResult());

        answerRepository.save(answer);

        return new ApiResponse("The answer edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<Answer> optionalAnswer = answerRepository.findById(id);

        if (optionalAnswer.isEmpty())
            return new ApiResponse("The answer not found", false);

        answerRepository.deleteById(id);
        return new ApiResponse("The answer deleted", true);
    }
}
