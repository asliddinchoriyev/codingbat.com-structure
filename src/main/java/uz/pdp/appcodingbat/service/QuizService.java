package uz.pdp.appcodingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.entity.Quiz;
import uz.pdp.appcodingbat.entity.Section;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.QuizDto;
import uz.pdp.appcodingbat.repository.AnswerRepository;
import uz.pdp.appcodingbat.repository.QuizRepository;
import uz.pdp.appcodingbat.repository.SectionRepository;
import uz.pdp.appcodingbat.repository.UserRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class QuizService {
    @Autowired
    QuizRepository quizRepository;
    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    AnswerRepository answerRepository;
    @Autowired
    UserRepository userRepository;

    public <Set> ApiResponse add(QuizDto quizDto) {

        boolean existsByNameAndId = quizRepository.existsByNameAndSection_Id(quizDto.getName(), quizDto.getSectionId());

        if (existsByNameAndId)
            return new ApiResponse("This quiz already added", false);

        Optional<Section> optionalSection = sectionRepository.findById(quizDto.getSectionId());
        if (optionalSection.isEmpty())
            return new ApiResponse("This section not found", false);

        Optional<Answer> optionalAnswer = answerRepository.findById(quizDto.getAnswerId());
        if (optionalAnswer.isEmpty())
            return new ApiResponse("This answer not found", false);

        java.util.Set<User> userSet = null;
        for (Integer userId : quizDto.getSolvedUserSet()) {
            Optional<User> optionalUser = userRepository.findById(userId);
            if (optionalUser.isEmpty())
                return new ApiResponse("This user not found", false);

            userSet.add(optionalUser.get());
        }

        Quiz quiz = new Quiz();
        quiz.setName(quizDto.getName());
        quiz.setSection(optionalSection.get());
        quiz.setAnswer(optionalAnswer.get());
        quiz.setSolvedUserSet(userSet);

        quizRepository.save(quiz);
        return new ApiResponse("The quiz added", true);
    }

    public Page<Quiz> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<Quiz> all = quizRepository.findAll(pageable);
        return all;
    }

    public Quiz getById(Integer id) {
        Optional<Quiz> optionalQuiz = quizRepository.findById(id);

        return optionalQuiz.orElse(null);
    }

    public ApiResponse edit(QuizDto quizDto, Integer id) {

        Optional<Quiz> optionalQuiz = quizRepository.findById(id);

        if (optionalQuiz.isEmpty())
            return new ApiResponse("The quiz not found", false);

        Optional<Section> optionalSection = sectionRepository.findById(quizDto.getSectionId());
        if (optionalSection.isEmpty())
            return new ApiResponse("This section not found", false);

        Optional<Answer> optionalAnswer = answerRepository.findById(quizDto.getAnswerId());
        if (optionalAnswer.isEmpty())
            return new ApiResponse("This answer not found", false);

        java.util.Set<User> userSet = null;
        for (Integer userId : quizDto.getSolvedUserSet()) {
            Optional<User> optionalUser = userRepository.findById(userId);
            if (optionalUser.isEmpty())
                return new ApiResponse("This user not found", false);

            userSet.add(optionalUser.get());
        }

        Quiz quiz = optionalQuiz.get();
        quiz.setName(quizDto.getName());
        quiz.setSection(optionalSection.get());
        quiz.setAnswer(optionalAnswer.get());
        quiz.setSolvedUserSet(userSet);

        quizRepository.save(quiz);

        return new ApiResponse("The quiz edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<Quiz> optionalQuiz = quizRepository.findById(id);

        if (optionalQuiz.isEmpty())
            return new ApiResponse("The quiz not found", false);

        quizRepository.deleteById(id);
        return new ApiResponse("The quiz deleted", true);
    }
}
