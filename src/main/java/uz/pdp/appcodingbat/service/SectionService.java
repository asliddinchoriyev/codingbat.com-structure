package uz.pdp.appcodingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.entity.Section;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.SectionDto;
import uz.pdp.appcodingbat.repository.LanguageRepository;
import uz.pdp.appcodingbat.repository.SectionRepository;

import java.util.Optional;

@Service
public class SectionService {
    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    LanguageRepository languageRepository;

    public ApiResponse add(SectionDto sectionDto) {

        boolean existsByNameAndId = sectionRepository.existsByNameAndLevelAndLanguage_Id(sectionDto.getName(), sectionDto.getLevel(), sectionDto.getLanguageId());

        if (existsByNameAndId)
            return new ApiResponse("This section already added", false);

        Optional<Language> optionalLanguage = languageRepository.findById(sectionDto.getLanguageId());
        if (optionalLanguage.isEmpty())
            return new ApiResponse("This language not found", false);

        Section section = new Section();
        section.setName(sectionDto.getName());
        section.setLevel(sectionDto.getLevel());

        section.setLanguage(optionalLanguage.get());

        sectionRepository.save(section);
        return new ApiResponse("The section added", true);
    }

    public Page<Section> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<Section> all = sectionRepository.findAll(pageable);
        return all;
    }

    public Section getById(Integer id) {
        Optional<Section> optionalSection = sectionRepository.findById(id);

        return optionalSection.orElse(null);
    }

    public ApiResponse edit(SectionDto sectionDto, Integer id) {

        Optional<Section> optionalSection = sectionRepository.findById(id);

        if (optionalSection.isEmpty())
            return new ApiResponse("The section not found", false);

        boolean byNameAndId = sectionRepository.existsByNameAndLevelAndLanguage_Id(sectionDto.getName(), sectionDto.getLevel(), sectionDto.getLanguageId());

        if (byNameAndId)
            return new ApiResponse("This section already added", false);

        Optional<Language> optionalLanguage = languageRepository.findById(sectionDto.getLanguageId());
        if (optionalLanguage.isEmpty())
            return new ApiResponse("This language not found", false);

        Section section = optionalSection.get();
        section.setName(sectionDto.getName());
        section.setLevel(sectionDto.getLevel());

        section.setLanguage(optionalLanguage.get());

        sectionRepository.save(section);

        return new ApiResponse("The section edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<Section> optionalSection = sectionRepository.findById(id);

        if (optionalSection.isEmpty())
            return new ApiResponse("The section not found", false);

        languageRepository.deleteById(id);
        return new ApiResponse("The section deleted", true);
    }
}
