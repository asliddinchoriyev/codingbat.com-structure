package uz.pdp.appcodingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.payload.ApiResponse;
import uz.pdp.appcodingbat.payload.UserDto;
import uz.pdp.appcodingbat.repository.LanguageRepository;
import uz.pdp.appcodingbat.repository.UserRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    LanguageRepository languageRepository;

    public ApiResponse add(UserDto userDto) {

        boolean existsByUsername = userRepository.existsByUsername(userDto.getUsername());

        if (existsByUsername)
            return new ApiResponse("This user already added", false);

        Set<Language> languageSet = null;

        for (Integer language : userDto.getLanguages()) {
            Optional<Language> optionalLanguage = languageRepository.findById(language);
            if (optionalLanguage.isEmpty())
                return new ApiResponse("This language not found", false);
            languageSet.add(languageRepository.getById(language));
        }

        User user = new User();
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setLanguages(languageSet);

        userRepository.save(user);
        return new ApiResponse("The user added", true);
    }

    public Page<User> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<User> all = userRepository.findAll(pageable);
        return all;
    }

    public User getById(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);

        return optionalUser.orElse(null);
    }

    public ApiResponse edit(UserDto userDto, Integer id) {

        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty())
            return new ApiResponse("The user not found", false);

        boolean existsByUsername = userRepository.existsByUsername(userDto.getUsername());

        if (existsByUsername)
            return new ApiResponse("This user already added", false);

        Set<Language> languageSet = null;

        for (Integer language : userDto.getLanguages()) {
            Optional<Language> optionalLanguage = languageRepository.findById(language);
            if (optionalLanguage.isEmpty())
                return new ApiResponse("This language not found", false);
            languageSet.add(languageRepository.getById(language));
        }

        User user = optionalUser.get();
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());

        user.setLanguages(languageSet);
        userRepository.save(user);
        return new ApiResponse("The user edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty())
            return new ApiResponse("The user not found", false);

        languageRepository.deleteById(id);
        return new ApiResponse("The user deleted", true);
    }
}
