package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appcodingbat.entity.Quiz;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Integer> {
    boolean existsByNameAndSection_Id(String name, Integer section_id);
}
