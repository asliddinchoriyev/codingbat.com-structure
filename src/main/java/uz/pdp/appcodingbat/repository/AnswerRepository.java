package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appcodingbat.entity.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    boolean existsByResult(String result);
}
